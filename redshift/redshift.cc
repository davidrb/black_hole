#include <iostream>
#include <array>
#include <cassert>

#include "gravity/physics.hpp"

#include <armadillo>

using namespace arma;

#include <iostream>

#include <SFML/Graphics.hpp>

int main() {
  auto rs = 150.0;

  auto E0 = 1.0;
  auto r0 = 151.0;
  auto p = Particle{};
  p.position = {0, r0, 0};
  p.velocity = {E0, (1-rs/r0)*E0, 0};

  auto g = schwarszchild_metric(rs);

  std::cout << "r\tE/E0\n";
  while(p.position(1) < 200.0) {
    parallel_transport(p.position, p.velocity, g, 1.0);
    std::cout << p.position(1) << "," << p.velocity(0)/E0 << std::endl;
  }

  return 0;
}
