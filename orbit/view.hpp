#ifndef VIEW_HPP
#define VIEW_HPP

#include <SFML/Graphics.hpp>
#include <sstream>
#include <iomanip>
#include "simulation.hpp"

struct View {
  View(auto& w, auto rs) :
    rs{rs}, window{w},
    satellite{7.0} {

    satellite.setFillColor(sf::Color::Red);
    radius.setOutlineThickness(1);
    radius.setFillColor(sf::Color(0.0, 0.0, 0.0, 0.0));
    radius.setOutlineColor(sf::Color(255, 255, 255));

    font.loadFromFile("arial.ttf");
    text.setFont(font);
    text.setCharacterSize(30);
  };

  double rs;

  sf::Text text;
  sf::Font font;
  sf::RenderWindow& window;
  sf::CircleShape radius;
  sf::CircleShape satellite;
};

void update_view(Simulation& sim, View& view) {
  auto size = view.window.getSize();
  auto r = sim.get_particle().position(1);
  auto phi = sim.get_particle().position(2);
  auto R = (1.0/200.0)*std::min(size.x, size.y)/2.0;

  view.radius.setRadius(R*view.rs);
  view.radius.setPosition(size.x/2 - R*150.0, size.y/2 - R*150.0);
  view.satellite.setPosition(size.x/2 + r*R*std::cos(phi) - 3.5, size.y/2 + r*R*std::sin(phi) - 3.5);
}

void render(Simulation& sim, View& view) {
  update_view(sim, view);
  view.window.draw(view.satellite);
  view.window.draw(view.radius);

  auto ss = std::stringstream{};
  ss.precision(2);
  ss << std::fixed;
  ss << "rate: " << sim.get_sim_rate() << "\n";
  ss << "proper time:\t" << sim.get_particle().proper_time << "s\n";
  ss << "coordinate time:\t" << sim.get_particle().position(0) << "s\n";
  ss << "radial component:\t" << sim.get_particle().position(1) << "ls\n";
  ss << "angular component:\t" << sim.get_particle().position(2) << "rads\n";

  view.text.setString(ss.str());
  view.window.draw(view.text);
}

#endif
