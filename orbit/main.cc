#include <iostream>
#include <array>
#include <cassert>
#include <iostream>
#include <chrono>

#include <SFML/Graphics.hpp>

#include "view.hpp"
#include "simulation.hpp"


int main() {
  auto r0 = 200.0, phi0 = 0.0;
  auto rdot0 = 0.0, phidot0 = 0.0015;
  auto rs = 150.0;

  auto sim = Simulation {
    {0.0,  r0, phi0},
    {1.0, rdot0, phidot0},
     rs, 1.0, 100.0
  };

  sf::RenderWindow window(sf::VideoMode(500, 500), "Black Hole Simulation");
  auto view = View{window, rs};

  using std::chrono::system_clock;
  using std::chrono::microseconds;
  using std::chrono::duration_cast;

  auto next_render = system_clock::now();

  while (window.isOpen()) {
    sf::Event event;

    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed)
        window.close();
    }

    sim.simulate();

    if(system_clock::now() >= next_render) {
      window.clear();
      render(sim, view);
      window.display();

      next_render = system_clock::now() + microseconds{16'666};
    }
  }

  return 0;
}
