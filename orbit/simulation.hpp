#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <chrono>
using std::chrono::system_clock;
using std::chrono::microseconds;
using std::chrono::duration_cast;

#include "gravity/physics.hpp"

class Simulation {
public:
  Simulation(vec const& position, vec const& velocity,
             double rs, double time_step, double time_rate) :
    metric{schwarszchild_metric(rs)},
    time_rate{time_rate},
    time_step{time_step} {

    particle.position = position;
    particle.velocity = velocity;
    last_update = system_clock::now();
  }

  void simulate() {
    auto micros = duration_cast<microseconds>(system_clock::now() - last_update);
    auto dt = micros.count()/1'000'000.0;

    while(dt > time_step/time_rate) {
      last_update = std::chrono::system_clock::now();
      free_fall(particle, metric, time_step);
      dt -= time_step;
      steps++;
    }
  }

  auto get_particle() const -> const auto& {
    return particle;
  }

  auto get_sim_rate() const {
    return (double)steps
            / duration_cast<microseconds>(system_clock::now() - start_time).count()
            * 1000000.0;
  }

private:
  Particle particle;
  std::function<mat(vec)> metric;

  decltype(system_clock::now()) last_update = system_clock::now();
  decltype(system_clock::now()) start_time = system_clock::now();
  double time_rate = 1000.0;
  double time_step = 1.0;
  long steps = 1u;
};

#endif
