#!/usr/bin/python
from matplotlib import pyplot, dates
from csv import reader
from dateutil import parser

with open('data.csv', 'r') as f:
    data = list(reader(f))

r = [float(i[0]) for i in data[1::]]
k = [float(i[1]) for i in data[1::]]

pyplot.xlabel('radius (ls)')
pyplot.ylabel('redshift')
pyplot.plot(r, k)
pyplot.savefig('redshift.svg')
pyplot.show()
