#include <armadillo>

using namespace arma;

// connection coefficients
auto christoffel(auto position, auto const& g) {
  auto gamma = cube{3, 3, 3};
  auto g_p = g(position);

  // calculate partial derivatives of metric components
  std::array<mat, 3u> dg_dx{};

  auto epsilon = 0.0001;
  auto delta = [=](auto i, auto j){ return i == j ? epsilon : 0.0; };

  for(auto i = 0u; i < 3u; i++) {
    auto dx = vec({delta(0u, i), delta(1u, i), delta(2u, i)});
    dg_dx[i] = (g(vec(position + dx)) - g(vec(position - dx)))/(2*epsilon);
  }

  // calculate inverse metric components
  auto g_i = mat(g_p.i());

  // calculate connection coefficients
  for(auto i = 0u; i < 3u; i++)
    for(auto j = 0u; j < 3u; j++)
      for(auto k = 0u; k < 3u; k++) {
        gamma(i, j, k) = 0.0;

        for(auto z = 0u; z < 3; z++)
          gamma(i, j, k) += 0.5*g_i(i, z)*(dg_dx[k](z, j) + dg_dx[j](z, k) - dg_dx[z](j, k));
      }

  return gamma;
}

void parallel_transport(auto& position, auto& velocity, auto g, double dt) {
  // update position
  position += velocity*dt/velocity(0);

  // update velocity
  auto gamma = christoffel(position, g);
  auto acceleration = vec({0, 0, 0});
  for(auto i = 0; i < 3; i++)
    for(auto j = 0; j < 3; j++)
      for(auto k = 0; k < 3; k++)
        acceleration(i) -= velocity(j)*velocity(k)*gamma(i, j, k);

  velocity += acceleration*dt/velocity(0);
}

void normalize_angle(auto& angle) {
  while (angle >= 2.0*3.141592654)
    angle -= 2.0*3.141592654;

  while (angle < 0)
    angle += 2.0*3.141592654;
}
