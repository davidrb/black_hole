#include <armadillo>

#include "math.hpp"

using namespace arma;

struct Particle {
  vec position;
  vec velocity;

  double proper_time = 0.0;
};

// move the particle along a geodesic
auto free_fall(Particle& p, auto const& g, double dt) {
  parallel_transport(p.position, p.velocity, g, dt);
  p.velocity /= p.velocity(0);

  // update proper time
  auto g_p = g(p.position);
  auto dtau_squared = 0.0;
  for(auto i = 0u; i < 3u; i++)
    dtau_squared += dt*dt*g_p(i, i)*p.velocity(i)*p.velocity(i);

  p.proper_time += std::sqrt(std::abs(dtau_squared));
  normalize_angle(p.position(2));
}


auto schwarszchild_metric(auto rs) {
  return [=](auto const& pos) -> Mat<double> {
    auto r = pos(1);
    auto x = 1.0-rs/r;
    return {{x, 0, 0}, {0, -1.0/x, 0}, {0, 0, -r*r}};
  };
}
